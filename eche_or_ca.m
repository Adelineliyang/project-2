function [eche,ca] = eche_or_ca(A)
% build a function to test whether a matrix is an echelon form or row canonical form;
% project 1.a

eche = 1; % if it is in echelon form, eche equals 1.
ca = 1; % if it is in row canonical form, eche equals 1.
[m,n] = size(A);
for i = 1:m
    for j = 1:n
        if (A(i,j) ~= 0)
            for k = (i+1):m
                if A(k,j) ~= 0
                    eche = 0;
                    break;
                end
            end
            if (i < m)
                i = i + 1; % continue to check nest row until it is the last row
            end
        end
        if (eche == 0)
            break;
        end
    end
    if (eche == 0)
        break;
    end
end
if (eche == 0)
    ca = 0; % a matrix which is not an echelon form is not a row canonical form either.
else
    for i = 1:m
        for j = 1:n
            if (A(i,j) ~= 0)
                if (A(i,j) ~= 1)
                    ca = 0;
                    break
                end
                for k = 1:i-1 
                    if (A(k,j) ~= 0)
                        ca = 0;
                        break;
                    end
                end
                if (i < m)
                    i = i + 1;
                end
            end
        end
        if (ca == 0)
            break
        end
    end
end
eche
ca
end