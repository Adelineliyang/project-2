function [A] = row_eche(A)
% return a row echelon form for any fiven input matrix
[m,n] = size(A);
for i = 1:m
    for j = 1:n           % step 1
        
        if A(i,j) ~= 0    % step 2 and step 5. Use pivot to zero out all rows below it.
            for k = (i+1):m
                A(k,:) = A(k,:) - (A(k,j)./A(i,j)).*A(i,:);
            end
            break;
            
            if A(i,j) == 0     % step 3. Exchange two rows.
                for k = (i+1):n
                    if A(k,j) ~= 0
                        A_ex = A(i,:);
                        A(i,:) = A(k,:);
                        A(k,:) = A_ex;
                        break
                    end
                end
                break
            end
            
            if A(:,j) == zeros(m,1)    % step 4
                j = j + 1;    % Columns with all zero represents a free variable,
                break         % and do not contain a pivot.
            end
            
            if i < m     % step 6. Continue to check if it's not the last row.
                i = i + 1;
            end
        end
    end
end
end         