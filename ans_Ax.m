function [X] = ans_Ax(A,b)
% return the solution to Ax=b.
% give appropriate errors if a solution is not possible.

[m,n] = size(A);
[brow,bcol] = size(b);
assert(m == brow,'A and b do not match');
assert(bcol == 1,'b is not a nx1 vector');
% make sure matrix argument A and vector argument b are match.

B = zeros(m,n+1);
B(:,1:n) = A;
B(:,n+1) = b; % build an augmented matrix B = [A_ca b].
[B_eche] = row_eche(B);     % convert any matrix B into row echelon form.
[B_ca] = row_ca(B_eche);    % convert B_eche into row canonical form.
rA = rank(A);
rB_ca = rank(B_ca);
X = ones(m,1);
if(rA < rB_ca)
    fprintf('solution is not possible');
    X = 'null'
end
if rA >= rB_ca         
    for i = 1:m
        for j = 1:n
            if A(i,j) ~= 0
                X = b./A;
            end
        end
    end
end
    return
end