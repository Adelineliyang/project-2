function [A] = row_ca(A)
% return a row canonical form for given matrix.
% return an appropriate error if the matrix is NOT in echelon form.

[m,n] = size(A);
[eche,ca] = eche_or_ca(A);
eche; % we need to check eche. 
assert(eche == 1,'The matrix is NOT in echelon form.');
% if eche ~= 1, we can draw conclution that it is not echelon. 
% if eche == 1, continue to run code below.

for i = m:-1:1
    while A(i,:) ~= 0 & i > 1
        i = i - 1; % begin with "i" as the last non-zero row
    end
    for j = 1:n
        if (A(i,j) ~= 0)
            A(i,:) = A(i,:)./A(i,j); % convert the leading entry of each row to "1"
            for k = 1:(i-1)
                A(k,:) = A(k,:) - A(k,j)*A(i,:); % back-substitution
            end
            if (i > 1)
                i = i - 1; % continue to check if it is the last row
            end
            if (i == 1)
                break % if "i" is 1, now the matrix is in row-canonical form
            end
        end
    end
end
end
