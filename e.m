% test code and plot
time = [];
for m = 10:10:1000
    n = 100;
    A = ones(m,n);
    b = ones(m,1);
    tic
    X = ans_Ax(A,b);
    time(m) = toc;
    figure(1)
    plot(m,time(m),'b.')
    xlabel('m');
    ylabel('time');
    hold on
end % m changes but n doesn't change.
for n = 10:10:1000
    m = 100;
    A = ones(m,n);
    b = ones(m,1);
    tic
    X = ans_Ax(A,b);
    time(n) = toc;
    figure(2)
    plot(n,time(n),'r.')
    xlabel('n');
    ylabel('time');
    hold on
end % n changes but m doesn't change.
for m = 1:300
    n = m;
    A = ones(m,n);
    b = ones(m,1);
    tic
    X = ans_Ax(A,b);
    time(n) = toc;
    figure(3)
    plot(m,time(m),'g.')
    xlabel('m and n');
    ylabel('time');
    hold on
end % n and m change together and m equals n.

