function [inv] = inverse(A)
% return the inverse of a given matrix
% or return an appropriate error message if the matrix is singular.
[row,column] = size(A);
augment_matrix = zeros(row,row+column);
augment_matrix(1:row,1:column) = A;
augment_matrix(1:row,column+1:column+row) = eye(row,row);
augment_matrix_eche = row_eche(augment_matrix);
augment_matrix_ca = row_ca(augment_matrix_eche);
r = rank(augment_matrix_ca);

if augment_matrix_ca(1:r,1:r) == eye(r,r)
    inv = augment_matrix_ca(1:row,r+1:row+column);
end
if augment_matrix_ca(1:r,1:r) ~= eye(r,r)
    error('the matrix is singular');
end
end