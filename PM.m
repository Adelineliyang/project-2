function [P,M] = PM(A)
% return P and M for any matrix.
[row,col] = size(A);
assert(row == col,'this is not a square matrix!');

P = eye(row,col);
for i = 1:row
    for j = 1:col
        
        if A(i,j) ~= 0    % Use pivot to zero out all rows below it.
            for k = (i+1):row
                A(k,:) = A(k,:) - (A(k,j)./A(i,j)).*A(i,:);
                P(k,:) = A(k,:) - (P(k,j)./P(i,j)).*P(i,:);
            end
            break;
            
            if A(i,j) == 0     % Exchange two rows.
                for k = (i+1):n
                    if A(k,j) ~= 0
                        A_ex = A(i,:);
                        A(i,:) = A(k,:);
                        A(k,:) = A_ex;
                        P_ex = P(i,:);
                        P(i,:) = P(k,:);
                        P(k,:) = P_ex;
                        break
                    end
                end
                break
            end
            
            if A(:,j) == zeros(m,1)    % step 4
                j = j + 1;    % Columns with all zero represents a free variable,
                break         % and do not contain a pivot.
            end
            
            if i < m     % step 6. Continue to check if it's not the last row.
                i = i + 1;
            end
        end
    end
end
M = eye(row,col);
for i = row:-1:1
    while A(i,:) ~= 0 & i > 1
        i = i - 1; % begin with "i" as the last non-zero row
    end
    for j = 1:col
        if (A(i,j) ~= 0)
            A(i,:) = A(i,:)./A(i,j); % convert the leading entry of each row to "1"
            for k = 1:(i-1)
                A(k,:) = A(k,:) - A(k,j)*A(i,:); % back-substitution
                M(k,:) = M(k,:) - M(k,j)*M(i,:);
            end
            if (i > 1)
                i = i - 1; % continue to check if it is the last row
            end
            if (i == 1)
                break % if "i" is 1, now the matrix is in row-canonical form
            end
        end
    end
end
P
M
end